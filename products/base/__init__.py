import json
import sys
import time
from threading import Condition, Lock
import datetime
import redis_queue
from helpers import *

environments =  \
    {
        "dev-sea": "https://api-sea.cpdev.dyson.com",
        "dev-004": "https://api.cpdev004.dyson.com",
        "si": "https://api.cpsi.dyson.com",
        "ci": "https://api.cpci.dyson.com",
        "uat": "https://api.cpuat.dyson.com",
        "lt": "https://api.cplt.dyson.com",
        "stage": "https://api.cpstage.dyson.com",
        "prd": "https://api.cp.dyson.com"
    }

class Base:
    """ Represents a Product of a particular type """
    connected = False
    mqttc = None

    def __init__(self, product_type, serial,  env, password="", broker = "", port = 443 ,cert_path = ""):
        # User inputs
        self.type = product_type
        self.serial = serial
        self.env = env
        self.cert_path = cert_path
        self.password = password
        self.broker = broker
        self.port = port

        # Topics
        self.topicBase = f"{self.type}/{self.serial}A"
        # Status: hello, good_bye, im_back
        self.status_connection_topic = f"{self.topicBase}/status/connection"
        # State: request_current_state, state_set_topic
        self.command_topic = f"{self.topicBase}/command"
        # Error: error, invalid_input
        self.status_faults_topic = f"{self.topicBase}/status/faults"
        # Debug data: log_data
        self.status_log_topic = f"{self.topicBase}/status/log"
        # upgrading_software
        self.status_software_topic = f"{self.topicBase}/status/software"

        if env == "prd" or env == "stage":
            self.ca_path = "./certs/DysonConnectedProductsRootCAProd.crt"
        elif env == "cn-prd" or env == "cn-stage":
            self.ca_path = "./certs/DysonConnectedProductsCNRootCA.crt"
        else:
            self.ca_path = "./certs/DysonConnectedProductsRootCA.crt"

        # System param
        self.q = redis_queue.RedisQueue(name='queue')
        self.condition = Condition()
        self.lock = Lock()

        print(f"Initialising a product {self.serial} of type {self.type}")

    """
        Helps to provision the product. 
        This will involve fetching the credentials to connect to the broker and 
        associating with a test user email and password. 
        After this has been completed, the device is said to be provisioned
    """
    def provision_product(self, email, password):
        print(f"Starting to provision device {self.serial} with email {email}")

        # Register the Device
        api_headers = {"Content-Type": "application/json"}
        api_url = "%s/v1/provisioningservice/deviceprovisioning/%sA" % (environments[self.env], self.serial)

        response = http_post(api_url, api_headers, self.ca_path, self.cert_path)
        if response.status_code > 300:
            print(f"Returned HTTP error from provisioning a product {response.status_code} ")
            sys.exit(-1)

        provisioned_info = json.loads(response.text)

        self.password = provisioned_info["TokenPassword"]
        self.broker = provisioned_info["BrokerHostName"]
        self.port = provisioned_info["BrokerPort"]

        print(f"Device credentials retrieved Serial:{self.serial} Password:{self.password} Broker:{self.broker}")

        # Register the Phone user
        print(f"Starting to register the App user {email}")

        api_url = "%s/v1/userregistration/authenticate?country=GB" % environments[self.env]

        # Need a registered user, boxing this to mc_test
        api_body = {"Email":"mc_test@dyson.com","Password":"Test123456"}
        response = http_post(api_url, api_headers, self.ca_path, body=api_body)
        if response.status_code > 300:
            print(f"Returned HTTP error from provisioning the user {response.status_code} ")
            sys.exit(-1)

        phone_data = json.loads(response.text)

        account = phone_data["Account"]
        password = phone_data["Password"]

        print(f"Registered the Phone user Token {account}")

        # Performing AVU call to complete connection journey
        print(f"Started a AVU request on the {self.serial} with the email {email}")

        mqtt_msg = '{"msg":"AUTHORISE-VERIFIED-USER","time":"%s","id":"%s","localpwd":"%s"}' % (datetime.date.today().isoformat(), account, password)
        mqtt_topic = f"{self.type}/{self.serial}/status/auth"

        mqtt_connect_publish(self.broker, self.port, mqtt_topic, mqtt_msg, self.serial + 'A', self.password, self.ca_path)

        print(f"Completed the provisioning of the {self.serial} with {email}")

    """
        Say Hello. 
        The product will Publish a Hello message to the broker.
        This HELLO message will be accompanied by the version information
    """
    def say_hello(self, version):
        self.connect()
        msg = {
            "msg": "HELLO",
            "time": datetime.date.today().isoformat(),
            "model": self.type,
            "version": version,
            "protocol": "1.0.0",
            "serialNumber": self.serial,
        }
        payload = json.dumps(msg)

        mmi = self.mqttc.publish(topic=self.status_connection_topic, payload=payload)
        mmi.wait_for_publish()
        print(f"Product {self.serial}A sent HELLO message {payload}")

    """
        Say Goodbye. 
        The product will Publish a Goodbye message to the broker.
        This GOODBYE message will be accompanied by time and reason information
    """
    def say_goodbye(self, reason):
        self.connect()
        msg = {
            "msg": "GOODBYE",
            "time": datetime.date.today().isoformat(),
            "reason": reason
        }
        payload = json.dumps(msg)

        mmi = self.mqttc.publish(topic=self.status_connection_topic, payload=payload)
        mmi.wait_for_publish()
        print(f"Product {self.serial}A sent GOODBYE message {payload}")

    """
        Im-back message. 
        The product will Publish a Im-back message to the broker.
        This IM-BACK message will be accompanied by time information
    """
    def say_im_back(self):
        self.connect()
        msg = {
            "msg": "IM-BACK",
            "time": datetime.date.today().isoformat()
        }
        payload = json.dumps(msg)

        mmi = self.mqttc.publish(topic=self.status_connection_topic, payload=payload)
        mmi.wait_for_publish()
        print(f"Product {self.serial}A sent IM-BACK message {payload}")

    """
        Report error message. 
        The product will Publish an error message to the broker.
        This ERROR message will be accompanied by time, value and reason information
    """
    def report_error(self, reason):
        self.connect()
        msg = {
            "msg": "ERROR",
            "time": datetime.date.today().isoformat(),
            "value": "GOODBYE", # TODO
            "reason": reason # TODO
        }
        payload = json.dumps(msg)

        mmi = self.mqttc.publish(topic=self.status_faults_topic, payload=payload)
        mmi.wait_for_publish()
        print(f"Product {self.serial}A sent ERROR message {payload}")

    """
        Report invalid input message. 
        The product will Publish an invalid input message to the broker.
        This INVALID-INPUT message will be accompanied by time
    """
    def report_invalid_input(self):
        self.connect()
        msg = {
            "msg": "INVALID-INPUT",
            "time": datetime.date.today().isoformat(),
            "received": "REQUEST-IS-INVALID" # TODO
        }
        payload = json.dumps(msg)

        mmi = self.mqttc.publish(topic=self.status_faults_topic, payload=payload)
        mmi.wait_for_publish()
        print(f"Product {self.serial}A sent ERROR message {payload}")

    """
        Requested log-data message. 
        The product will Publish a log-data message to the broker.
        This LOG-DATA message will be accompanied by time, file and data information
    """
    def log_data (self, file=None, data=None):
        self.connect()
        msg = {
            "msg": "LOG-DATA",
            "time": datetime.date.today().isoformat(),
            "file": file,
            "data": data
        }
        payload = json.dumps(msg)

        mmi = self.mqttc.publish(topic=self.status_log_topic, payload=payload)
        mmi.wait_for_publish()
        print(f"Product {self.serial}A sent LOG-DATA message {payload}")

    """
        Report a software upgrading message
    """
    def report_upgrading_software(self, status):
        self.connect()
        msg = {
            "msg": "UPGRADING-SOFTWARE",
            "time": datetime.date.today().isoformat(),
            "state": status,
            "code": "-2001"
        }
        payload = json.dumps(msg)

        mmi = self.mqttc.publish(topic=self.status_software_topic, payload=payload)
        mmi.wait_for_publish()
        print(f"Product {self.serial}A sent UPGRADING-SOFTWARE message {payload}")

    """
       Wait for a command.
       This command is sent by the App via the broker.
    """
    def wait_for_command(self, msg_type, payload=None):
        t1 = time.time()
        self.wait()
        t2 = time.time()
        delta = time_delta(t1, t2)

        payload = json.loads(self.q.get())
        if msg_type == payload["msg"]:
            print(f'Received command {msg_type} for serial {self.serial} Time take {delta} ms')

    """ Private APIs """
    def connect(self):
        self.lock.acquire()
        if self.connected is True:
            self.lock.release()
            return
        else:
            self.lock.release()
            self.mqttc = mqtt_connect(self.broker,
                                      self.port,
                                      self.serial+"A",
                                      self.password,
                                      self.ca_path,
                                      self.on_connect,
                                      self.on_disconnect)

            self.wait()

            self.mqttc.subscribe(topic=self.command_topic)
            self.mqttc.message_callback_add(self.command_topic, self.on_message)
        return

    def wait(self, timeout=30):
        self.condition.acquire()
        result = self.condition.wait(timeout)
        self.condition.release()
        return result

    def notify(self):
        self.condition.acquire()
        self.condition.notify()
        self.condition.release()

    def on_connect(self, client, userdata, flags_dict, result):
        self.lock.acquire()
        if self.connected is False:
            self.connected = True
            print(f'Device {self.serial} connected successfully to the broker')
            self.notify()
        self.lock.release()

    def on_disconnect(self, client, userdata, rc):
        print(f"{self.serial} got disconnected")
        self.lock.acquire()
        self.connected = False
        self.lock.release()

    def handle_software_upgrade(self):
        pass
    
    def handle_request_current_state(self):
        pass
      
    def handle_state_set(self):
        pass

    def response_switch(self, message):
        cases = {
            "SOFTWARE-UPGRADE": self.handle_software_upgrade,
            "REQUEST-CURRENT-STATE": self.handle_request_current_state,
            "STATE-SET": self.handle_state_set
        }
        
        handler = cases.get(message)

        if handler is not None:
            handler()
            return True
        
        return False

    def on_message(self, client, userdata, message):
        is_valid_message = self.response_switch(message.payload.msg)

        if is_valid_message:
            self.q.put(message.payload)
            self.notify()

    def start(self, identifier, interval, life_time):
        pass