import json
import datetime
import base64
import time
import random
import cbor2
from products.base import Base
from task import Task


class Product_520(Base):

    def __init__(self, product_type, serial,  env, password="", broker = "", port = 443 ,cert_path = ""):
        Base.__init__(self, product_type, serial,  env, password="", broker="", port=443 ,cert_path="")

        # env_data
        self.environmental_task = None
        self.command_task = None
        self.status_summary_topic = f"{self.topicBase}/status/summary"

    def report_env_data(self):
        self.connect()
        msg = Product_520.gen_env_message()

        cbor_payload = base64.b64encode(cbor2.dumps(msg, datetime_as_timestamp=True, timezone=cbor2.compat.timezone.utc)).decode('utf-8')
        json_payload = json.dumps(msg)

        mmi = self.mqttc.publish(topic=self.status_summary_topic, payload=cbor_payload)
        mmi.wait_for_publish()
        print(f"Product {self.serial}A sent ENV_DATA message {json_payload}")

    def start(self, identifier, interval, life_time):
        super().start(identifier, interval, life_time)

        self.environmental_task = Task('environmental_task', interval, life_time)
        self.command_task = Task('command_task', interval, life_time)

        self.environmental_task.start()
        self.command_task.start()

    def stop(self):
        self.environmental_task.join()
        self.command_task.join()
        return True

    @staticmethod
    def gen_env_message():
        data = {}
        cfg = {
            'aqlm': {'min': 0, 'max': 990},
            'volm': {'min': 0, 'max': 990},
            'no2m': {'min': 0, 'max': 990},
            'p25m': {'min': 0, 'max': 9999},
            'p10m': {'min': 0, 'max': 9999},
            'tmpm': {'min': 2430, 'max': 3530},
            'fnsp': {'min': 0, 'max': 100},
            'humm': {'min': 0, 'max': 990},
            'fnmd': {'min': 1, 'max': 5},
            'fnau': {'min': 0, 'max': 60},
            'fnon': {'min': 0, 'max': 60}
        }
        max_buckets = 20

        for k, v in cfg.items():
            bucket = [0] * max_buckets
            for i in range(max_buckets):
                value = random.randint(v['min'], v['max'])
                if value % 50 == 0: value = -1
                bucket[i] = value
            data[k] = bucket

        data["msg"] = "ENV-DATA"
        data["time"] = datetime.date.today().isoformat()
        data["tmsp"] = int(time.time())

        return data