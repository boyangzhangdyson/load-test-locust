from redis import ConnectionPool, Redis
import json, urllib3, os
import boto3
from locust import TaskSet, Locust, task, events
import time
from locust.exception import StopLocust

import requests

redis_server = os.environ['MASTER_IP'] if 'MASTER_IP' in os.environ else 'localhost'

urllib3.disable_warnings(urllib3.exceptions.SecurityWarning)
redis_conn_pool = ConnectionPool(host=redis_server, port=6379)
Redis(connection_pool=redis_conn_pool).set("ota:loadtest-deviceid:product", 0)

config = json.load(open('config-lt.json'))
session = boto3.session.Session(
    aws_access_key_id=config['AWS_ACCESS_KEY_ID'],
    aws_secret_access_key=config['AWS_SECRET_ACCESS_KEY'],
    region_name='eu-west-1')

dynamodb = session.resource('dynamodb')
serverCert = config['SERVER_CERT_PATH']


class ConnectError(Exception):
    def __init__(self, message):
        super(ConnectError, self).__init__(message)


class ResponseError(Exception):
    def __init__(self, message):
        super(ResponseError, self).__init__(message)


def fire_failure(request_type, name, response_time, exception, **kwargs):
    events.request_failure.fire(
        request_type=request_type, name=name, response_time=response_time, exception=exception, **kwargs)


def fire_success(request_type, name, response_time=0, response_length=0, **kwargs):
    events.request_success.fire(
        request_type=request_type, name=name, response_time=response_time, response_length=response_length, **kwargs)

def time_delta(t1, t2):
    return int((t2 - t1) * 1000)


class AppBehaviour(TaskSet):

    def log(self, msg=''):
        print(f'{msg}')

    def on_start(self):
        fire_success('Client', 'App')

    def end(self):
        raise StopLocust()

    def call_api(self, version, platform, locale, guid, count):
        now = time.time()
        api = f"remoteconfig"
        resp = requests.get(
            f"https://api.cplt.dyson.com/api/v1/remoteconfig?appVersion={version}&locale={locale}&guid={guid}",
            headers={'User-Agent': platform},
            verify=serverCert)
        if resp.status_code == 200:
            fire_success('HTTP', api, time_delta(now, time.time()), 0)
        elif resp.status_code == 204:
            fire_success('HTTP', api, time_delta(now, time.time()), 0)
        elif resp.status_code == 503:
            if count == 3:
                fire_failure('HTTP', api, time_delta(now, time.time()), ResponseError(f'Error:{resp.status_code}'))
            else:
                count = count + 1
                self.call_api('1.1', 'Android', 'en_GB', '65b398f3efc84663b1ce14f06afb3751', count)
        else:
            fire_failure('HTTP', api, time_delta(now, time.time()), ResponseError(f'Error:{resp.status_code}'))

    @task
    def get_remote_config(self):
        self.call_api('Testcc', 'Android','en_GB', '65b398f3efc84663b1ce14f06afb3751', 0)


class AppLocust(Locust):
    task_set = AppBehaviour
