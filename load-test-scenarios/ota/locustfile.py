from redis import ConnectionPool, Redis
import json, datetime, urllib3
import boto3
from locust import TaskSet, Locust, task, events
from locust.exception import StopLocust
import paho.mqtt.client as mqtt
from threading import Condition
import gevent
import time

urllib3.disable_warnings(urllib3.exceptions.SecurityWarning)
redis_conn_pool = ConnectionPool(host="localhost", port=6379)
Redis(connection_pool=redis_conn_pool).set("ota:loadtest:deviceid", 0)

config = json.load(open('./config-lt.json'))
session = boto3.session.Session(
    aws_access_key_id=config['AWS_ACCESS_KEY_ID'],
    aws_secret_access_key=config['AWS_SECRET_ACCESS_KEY'],
    region_name='eu-west-1')

dynamodb = session.resource('dynamodb')

# UPDATE asset.Assets SET CurrentReleaseId = NULL, PendingReleaseId = NULL, LimitedFunctionality = NULL where ModelId = 6;
# DELETE FROM asset.Upgrades WHERE ReleaseId = 1 OR ReleaseId = 2;

MQTT_NOT_CONNECTED = 100


class ConnectError(Exception):
    def __init__(self, message):
        super(ConnectError, self).__init__(message)


def fire_failure(request_type, name, response_time, exception, **kwargs):
    events.request_failure.fire(
        request_type=request_type, name=name, response_time=response_time, exception=exception, **kwargs)


def fire_success(request_type, name, response_time=0, response_length=0, **kwargs):
    events.request_success.fire(
        request_type=request_type, name=name, response_time=response_time, response_length=response_length, **kwargs)


def time_delta(t1, t2):
    return int((t2 - t1) * 1000)


class Behaviour(TaskSet):

    def wait(self, timeout=300):
        self.condition.acquire()
        result = self.condition.wait(timeout)
        self.condition.release()
        return result

    def notify(self):
        self.condition.acquire()
        self.condition.notify()
        self.condition.release()

    def on_connect(self, client, userdata, flags_dict, result):
        self.connection_status = result
        self.notify()

    def on_message(self, client, userdata, message):
        if b"SOFTWARE-UPGRADE" in message.payload:
            self.notify()

    def report_upgrading_software(self, status):
        msg = {
            "msg": "UPGRADING-SOFTWARE",
            "time": datetime.date.today().isoformat(),
            "state": status,
            "code": "-2001"
        }
        payload = json.dumps(msg)

        mmi = self.mqttc.publish(topic=self.upgrading_topic, payload=payload)
        mmi.wait_for_publish()

    def say_hello(self, version):

        msg = {
            "msg": "HELLO",
            "time": datetime.date.today().isoformat(),
            "model": self.productType,
            "version": version,
            "protocol": "1.0.0"
        }
        payload = json.dumps(msg)
        name = f"publish:HELLO:{version}"

        mmi = self.mqttc.publish(topic=self.hello_topic, payload=payload)
        mmi.wait_for_publish()

        fire_success('MQTT', name)

    def init(self):
        idx = Redis(connection_pool=redis_conn_pool).incr("ota:loadtest:deviceid")
        record = dynamodb.Table('LoadTest_Data').get_item(Key={'id': idx})

        # if 'Item' not in record:
        #     raise Exception('No more agents required')

        self.idx = idx

        self.data = json.loads(record['Item']['data'])
        self.username = self.data['productSerial']
        self.password = self.data['tokenPassword']
        self.host = self.data['broker']
        self.serialNumber = self.data['productSerial']
        self.productType = self.data['product']
        self.topicBase = f"{self.productType}/{self.serialNumber}"
        self.ca_cert = config['SERVER_CERT_PATH']
        self.hello_topic = f"{self.topicBase}/status/connection"
        self.upgrading_topic = f"{self.topicBase}/status/software"
        self.upgrade_topic = f"{self.topicBase}/command"
        self.client_id = self.data['productSerial']

        self.condition = Condition()

    def connect(self):
        self.mqttc = mqtt.Client(client_id=self.data['productSerial'], protocol=mqtt.MQTTv31)

        self.mqttc.tls_set(self.ca_cert)
        self.mqttc.tls_insecure_set(True)
        self.mqttc.username_pw_set(self.username, self.password)

        self.mqttc.on_connect = self.on_connect

        self.connection_status = MQTT_NOT_CONNECTED

        [host, port] = self.host.split(":")
        result = self.mqttc.connect(host, int(port))

        self.mqttc.loop_start()
        self.loop_started = True

        self.wait()

        if self.connection_status != mqtt.MQTT_ERR_SUCCESS:
            fire_failure('MQTT', 'connect', 0, ConnectError(f'Error:{self.connection_status}'))
            self.end()
        else:
            fire_success('MQTT', 'connect')

    def end(self):

        if self.loop_started: self.mqttc.loop_stop()
        if self.connection_status == mqtt.MQTT_ERR_SUCCESS: self.mqttc.disconnect()

        raise StopLocust()

    @task
    def task_to_run(self):

        self.init()

        self.connect()

        self.mqttc.subscribe(topic=self.upgrade_topic)
        self.mqttc.message_callback_add(self.upgrade_topic, self.on_message)

        self.say_hello('LT_SOP_VERSION6666666666')
        self.start_time = time.time()

        if not self.wait():
            fire_failure('OP', 'upgrade-available', 0, ConnectError(f'Error:Timeout'))
            print(f"{self.idx}:{self.serialNumber}: Timeout while waiting for upgrade")
        else:
            fire_success('OP', 'upgrade-available', time_delta(self.start_time, time.time()), 0)

            self.say_hello('LT.HK.520.INSTANT.PR')
            gevent.sleep(1)

        self.end()


class DyClient(Locust):
    task_set = Behaviour
