from redis import ConnectionPool, Redis, StrictRedis
import redis_queue
import json, datetime, urllib3
import boto3
from locust import TaskSet, Locust, task, events
from locust.exception import StopLocust
import mqtt.client as mqtt
from threading import Condition
import gevent
import time
from random import *

urllib3.disable_warnings(urllib3.exceptions.SecurityWarning)
redis_conn_pool = ConnectionPool(host="localhost", port=6379)
Redis(connection_pool=redis_conn_pool).set("ota:loadtest:deviceid", 0)

config = json.load(open('./config-lt.json'))
session = boto3.session.Session(
    aws_access_key_id=config['AWS_ACCESS_KEY_ID'],
    aws_secret_access_key=config['AWS_SECRET_ACCESS_KEY'],
    region_name='eu-west-1')

dynamodb = session.resource('dynamodb')

# UPDATE asset.Assets SET CurrentReleaseId = NULL, PendingReleaseId = NULL, LimitedFunctionality = NULL where ModelId = 6;
# DELETE FROM asset.Upgrades WHERE ReleaseId = 1 OR ReleaseId = 2;

MQTT_NOT_CONNECTED = 100


class ConnectError(Exception):
    def __init__(self, message):
        super(ConnectError, self).__init__(message)


def fire_failure(request_type, name, response_time, exception, **kwargs):
    events.request_failure.fire(
        request_type=request_type, name=name, response_time=response_time, exception=exception, **kwargs)


def fire_success(request_type, name, response_time=0, response_length=0, **kwargs):
    events.request_success.fire(
        request_type=request_type, name=name, response_time=response_time, response_length=response_length, **kwargs)


def time_delta(t1, t2):
    return int((t2 - t1) * 1000)


class AppBehaviour(TaskSet):

    def wait_for_notify(self, timeout=3000):
        self.condition.acquire()
        result = self.condition.wait(timeout)
        self.condition.release()
        return result

    def log(self, msg=''):
        print(f'{self.idx}:{self.serialNumber}: {msg}')

    def notify(self):
        self.condition.acquire()
        self.condition.notify()
        self.condition.release()

    def on_connect(self, client, userdata, flags_dict, result):
        self.connection_status = result
        self.notify()

    def on_disconnect(self, client, userdata, rc):
        self.log('App Disconnected')
        self.end()

    def on_message(self, client, userdata, message):
        if b"ENVIRONMENTAL-CURRENT-SENSOR-DATA" in message.payload:
            self.notify()

    def init(self):
        self.q = redis_queue.RedisQueue(name='devices')
        self.idx = int(self.q.get().decode('ascii'))

        record = dynamodb.Table('LoadTest_Data').get_item(Key={'id': self.idx})

        self.data = json.loads(record['Item']['data'])
        self.username = self.data['accountGuid']
        self.password = self.data['accountPassword']
        self.host = self.data['broker']
        self.serialNumber = self.data['productSerial']
        self.productType = self.data['product']
        self.topicBase = f"{self.productType}/{self.serialNumber}"
        self.ca_cert = config['SERVER_CERT_PATH']
        self.command_topic = f"{self.topicBase}/command"
        self.status_topic = f"{self.topicBase}/status/current"
        self.client_id = 'i-' + str(self.idx) + '-' + str(randint(1,99999))

        self.condition = Condition()


    def connect(self):
        self.mqttc = mqtt.Client(client_id=self.client_id, protocol=mqtt.MQTTv31)

        self.mqttc.tls_set(self.ca_cert)
        self.mqttc.tls_insecure_set(True)
        self.mqttc.username_pw_set(self.username, self.password)

        self.mqttc.on_connect = self.on_connect
        self.mqttc.on_disconnect = self.on_disconnect

        self.connection_status = MQTT_NOT_CONNECTED

        [host, port] = self.host.split(":")
        result = self.mqttc.connect(host, int(port))

        self.mqttc.loop_start()
        self.loop_started = True

        self.wait_for_notify()

        self.mqttc.subscribe(topic=self.status_topic)
        self.mqttc.message_callback_add(self.status_topic, self.on_message)

        return self.connection_status == mqtt.MQTT_ERR_SUCCESS

    def end(self):
        if self.loop_started: self.mqttc.loop_stop()
        if self.connection_status == mqtt.MQTT_ERR_SUCCESS: self.mqttc.disconnect()

        raise StopLocust()

    def on_start(self):
        self.init()

        retry_count = 5
        while True:
            if not self.connect():
                retry_count -= 1
                if retry_count > 0:
                    gevent.sleep(1)
                    continue
                break
            break

        if self.connection_status == mqtt.MQTT_ERR_SUCCESS:
            self.log(f'App Connected {self.client_id}')
            fire_success('MQTT', 'App connect')
        else:
            fire_failure('MQTT', 'connect', 0, ConnectError(f'Error:{self.connection_status}'))
            self.log(f'ConnectError:{self.connection_status}')
            self.end()

        self.start_time = time.time()

    @task
    def request_sensor_data(self):
        msg = {
            "time": datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S%z') + 'Z',
            "msg": "REQUEST-PRODUCT-ENVIRONMENT-CURRENT-SENSOR-DATA"
        }

        mmi = self.mqttc.publish(topic=self.command_topic, payload=json.dumps(msg))
        mmi.wait_for_publish()
        fire_success('MQTT', f"publish:REQUEST-PRODUCT-ENVIRONMENT-CURRENT-SENSOR-DATA")
        self.wait_for_notify()


class ProductBehaviour(TaskSet):
    def wait_for_notify(self, timeout=30):
        self.condition.acquire()
        result = self.condition.wait(timeout)
        self.condition.release()
        return result

    def log(self, msg=''):
        print(f'{self.idx}:{self.serialNumber}: {msg}')

    def notify(self):
        self.condition.acquire()
        self.condition.notify()
        self.condition.release()

    def on_connect(self, client, userdata, flags_dict, result):
        self.connection_status = result
        self.notify()

    def on_disconnect(self, client, userdata, rc):
        self.log('Product Disconnected')
        self.end()

    def on_message(self, client, userdata, message):
        if b"REQUEST-PRODUCT-ENVIRONMENT-CURRENT-SENSOR-DATA" in message.payload:
            self.notify()

    def init(self):
        self.q = redis_queue.RedisQueue(name='devices')
        self.idx = Redis(connection_pool=redis_conn_pool).incr("ota:loadtest:deviceid")

        record = dynamodb.Table('LoadTest_Data').get_item(Key={'id': self.idx})

        self.data = json.loads(record['Item']['data'])
        self.username = self.data['productSerial']
        self.password = self.data['tokenPassword']
        self.host = self.data['broker']
        self.serialNumber = self.data['productSerial']
        self.productType = self.data['product']
        self.topicBase = f"{self.productType}/{self.serialNumber}"
        self.ca_cert = config['SERVER_CERT_PATH']
        self.command_topic = f"{self.topicBase}/command"
        self.status_topic = f"{self.topicBase}/status/current"
        self.hello_topic = f"{self.topicBase}/status/connection"
        self.client_id = self.data['productSerial']

        self.condition = Condition()
        self.log(self.idx)


    def connect(self):
        self.mqttc = mqtt.Client(client_id=self.data['productSerial'], protocol=mqtt.MQTTv31)

        self.mqttc.tls_set(self.ca_cert)
        self.mqttc.tls_insecure_set(True)
        self.mqttc.username_pw_set(self.username, self.password)

        self.mqttc.on_connect = self.on_connect
        self.mqttc.on_disconnect = self.on_disconnect

        self.connection_status = MQTT_NOT_CONNECTED

        [host, port] = self.host.split(":")
        result = self.mqttc.connect(host, int(port))
        self.mqttc.loop_start()
        self.loop_started = True

        self.wait_for_notify()

        self.mqttc.subscribe(topic=self.command_topic)
        self.mqttc.message_callback_add(self.command_topic, self.on_message)

        return self.connection_status == mqtt.MQTT_ERR_SUCCESS

    def end(self):
        if self.loop_started: self.mqttc.loop_stop()
        if self.connection_status == mqtt.MQTT_ERR_SUCCESS: self.mqttc.disconnect()

        raise StopLocust()

    def on_start(self):
        self.init()

        retry_count = 5
        while True:
            if not self.connect():
                retry_count -= 1
                if retry_count > 0:
                    gevent.sleep(1)
                    continue
                break
            break

        if self.connection_status == mqtt.MQTT_ERR_SUCCESS:
            fire_success('MQTT', 'Product connect')
            self.q.put(self.idx)
        else:
            #fire_failure('MQTT', 'connect', 0, ConnectError(f'Error:{self.connection_status}'))
            self.log(f'ConnectError:{self.connection_status}')
            self.end()

        self.start_time = time.time()

    @task
    def wait_for_request(self):
        self.wait_for_notify()
        msg = {
            "msg": "ENVIRONMENTAL-CURRENT-SENSOR-DATA",
            "time":datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S%z') + 'Z',
            "data":{"tact":"2894","hact":"0054","pact":"0004","vact":"0000","sltm":"OFF"}
        }
        mmi = self.mqttc.publish(topic=self.status_topic, payload=json.dumps(msg))
        mmi.wait_for_publish()
        fire_success('MQTT', f"publish:ENVIRONMENTAL-CURRENT-SENSOR-DATA")


class PrdClient(Locust):
    task_set = ProductBehaviour
    min_wait = 500
    max_wait = 500


class AppClient(Locust):
    task_set = AppBehaviour
    min_wait = 4000
    max_wait = 8000


