import redis_queue


size = redis_queue.RedisQueue("devices").qsize()

if size != 0:
    print(f"Have {size} Messages in the queue.. Deleting them ..")
    redis_queue.RedisQueue("devices").empty_queue()
    if redis_queue.RedisQueue("devices").empty():
        print(f"All message from devices deleted")
    else:
        size = redis_queue.RedisQueue("devices").qsize()
        print(f"Residue remains {size} .. Try Again")
