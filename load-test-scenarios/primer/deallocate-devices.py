
from redis import ConnectionPool, Redis
import requests, json
import boto3
import logging
import threading

import urllib3
urllib3.disable_warnings(urllib3.exceptions.SecurityWarning)

logging.basicConfig(filename="./app.log", level=logging.INFO)

config = json.load(open('config-lt.json'))
session = boto3.session.Session(aws_access_key_id=config['AWS_ACCESS_KEY_ID'], aws_secret_access_key=config['AWS_SECRET_ACCESS_KEY'])
dynamodb = session.resource('dynamodb')

REDIS_HOST = "localhost"
REDIS_PORT = 6379
REDIS_KEY = "ota:loadtest:deviceid"

redis_conn_pool = ConnectionPool(host=REDIS_HOST, port=REDIS_PORT)

Redis(connection_pool=redis_conn_pool).set(REDIS_KEY, 0)


def get_item():
    idx = Redis(connection_pool=redis_conn_pool).incr(REDIS_KEY)
    item = dynamodb.Table('LoadTest_Data').get_item(Key={'id': idx})['Item']

    return idx, item


class Runner(threading.Thread):

    def __init__(self, id):
        threading.Thread.__init__(self)
        self.id = id

    def deallocate(self):
        idx, item = get_item()

        if item is None:
            return False

        data = json.loads(item['data'])

        resp = requests.request("DELETE",
                                f"http://provisioning.lt.connected.dyson.cloud/api/deviceprovisioning/{data['productSerial']}")
        logging.error(f"{data['productSerial']}|{resp.status_code}")

        return True

    def run(self):
        while True:
            if not self.deallocate():
                break


threads = []
tid = 1
for x in range(25):
    t = Runner(tid)
    tid += 1
    t.start()
    threads.append(t)

for t in threads:
    t.join()