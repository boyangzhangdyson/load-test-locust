import datetime
import json
import threading
import time

import boto3

config = json.load(open('../../config-lt.json'))
session = boto3.session.Session(aws_access_key_id=config['AWS_ACCESS_KEY_ID'], aws_secret_access_key=config['AWS_SECRET_ACCESS_KEY'])

db_client = session.client('dynamodb')
dynamodb = session.resource('dynamodb')
deviceIdx = 0
table_name = 'LoadTest_Data'


def create_table():

    try:
        db_client.describe_table(TableName=table_name)

    except db_client.exceptions.ResourceNotFoundException:

        db_client.create_table(
            TableName=table_name,
            KeySchema=[
                {
                    'AttributeName': 'id',
                    'KeyType': 'HASH'
                }
            ],
            AttributeDefinitions=[
                {
                    'AttributeName': 'id',
                    'AttributeType': 'N'
                }
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 100,
                'WriteCapacityUnits': 500
            }
        )

    db_client.get_waiter('table_exists').wait(TableName=table_name)

    print('Tabled created')

    return dynamodb.Table(table_name)


def reset_table_capacity():

    db_client.update_table(
        TableName=table_name,
        ProvisionedThroughput={
            'ReadCapacityUnits': 100,
            'WriteCapacityUnits': 20
        })


def generate_id():
    global deviceIdx

    char_numbers, numbers = divmod(deviceIdx, 10000)
    char1num, char2num = divmod(char_numbers, 10)
    char1 = chr(ord('A') + char1num)
    char2 = chr(ord('A') + char2num)

    deviceIdx += 1

    return f'L{char1}{char2}{numbers:04}'


def generate_user(product):

    data = {}

    data['password'] = "p455w0rd"
    data['country'] = "SG"
    data['culture'] = "en-SG"
    data['honorific'] = "Mr"
    data['contactPermitted'] = True
    data['firstName'] = 'Load'
    data['lastName'] = 'Tester'
    data['location'] = {"2532627":  "Abbey Wood"}
    data['product'] = product

    # NM7-US-HJA1448A
    id = generate_id()
    if product == '520':
        data['purchaseDate'] = datetime.date(2016, 1, 4).isoformat()
        data['productSerial'] = 'VW2-SG-{}A'.format(id)
        data['email'] = 'n469_SS_{}@dyson.com'.format(id)

    elif product == '552':
        data['purchaseDate'] = datetime.date(2016, 1, 4).isoformat()
        data['productSerial'] = 'YR2-SG-{}A'.format(id)
        data['email'] = 'n552_SS_{}@dyson.com'.format(id)

    else:
        data['purchaseDate'] = datetime.date(2015, 9, 14).isoformat()
        data['productSerial'] = 'AC0-SG-{}A'.format(id)
        data['email'] = 'n223_SS_{}@dyson.com'.format(id)

    return data


def insert_items(table, items, idx):
    i = 1
    with table.batch_writer() as batch:
        for item in items:
            batch.put_item(Item=item)
            print('Item {}.{} inserted'.format(idx, i))
            i += 1


start = time.time()

table = create_table()
items = []

for x in range(45000):
    data = generate_user('520')
    items.append(
        {
            'id': x + 1,
            'serialNumber': data['productSerial'],
            'data': json.dumps(data)
        }
    )

chunks = [items[x:x+1000] for x in range(0, len(items), 1000)]

print(f'Items prepared ({round(time.time()-start)} secs)')

class Runner(threading.Thread):
    def __init__(self, id, table, items):
        threading.Thread.__init__(self)
        self.id = id
        self.table = table
        self.items = items

    def run(self):
        insert_items(self.table, self.items, self.id)


threads = []
tid = 1
for chunk in chunks:
    t = Runner(tid, table, chunk)
    tid += 1
    t.start()
    threads.append(t)

for t in threads:
    t.join()

reset_table_capacity()

print(f'Completed ({round(time.time()-start)} secs)')