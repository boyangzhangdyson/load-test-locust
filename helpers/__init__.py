import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
import requests
from random import *


def http_post(url, headers, ca_path, cert_path=None, body=None, verbose=False):
    if verbose:
        print(f"POST call {url}")
        print(f"Headers: {headers}")
        if body is not None:
            print(f"Body:{body}")

    return requests.post(url, json=body, headers=headers, verify=ca_path, cert=cert_path)


def mqtt_connect_publish(host, port, topic, payload, username, password, ca_path, verbose=False):
    auth = {"username": username, "password": password}
    ssl_opts = {"ca_certs": ca_path}

    if verbose:
        print("-Publishing to %s:%s, on topic %s, with username: %s, password: %s" % (
        host, port, topic, auth["username"], auth["password"]))

    publish.single(topic, protocol=mqtt.MQTTv31, payload=payload, hostname=host, port=port, auth=auth, tls=ssl_opts)


def mqtt_connect(host, port, username, password, ca_path, on_connect=False, on_disconnect=False, verbose=False):
    if verbose:
        print(f"-Mqtt Connect with username: {username} and password: {password}")

    mqttc = mqtt.Client(client_id=username + "-" +str(randint(1,99999)), protocol=mqtt.MQTTv31)
    mqttc.tls_set(ca_path)
    mqttc.tls_insecure_set(True)
    mqttc.username_pw_set(username, password)

    mqttc.on_connect = on_connect
    mqttc.on_disconnect = on_disconnect
    mqttc.connect(host, port)

    mqttc.loop_start()

    return mqttc


def time_delta(t1, t2):
    return int((t2 - t1) * 1000)