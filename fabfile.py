import sys
from fabric.api import *
from fabric.contrib.project import rsync_project
from fabric.context_managers import shell_env

env.disable_known_hosts = False
env.reject_unknown_hosts = False
env.key_filename = '/Users/aaga/work/connected_lt_key.pem'
env.user = 'ubuntu'

master = '10.49.153.34'
slaves = [
    '10.49.153.41',
    '10.49.153.43',
    '10.49.153.10',
    '10.49.153.62'
]

all_instances = [master] + slaves

locust_opts = '--logfile=locust.log --no-reset-stats'

def run_bg(command):
    run('nohup %s &> /dev/null &' % command, pty=False)


@parallel
@hosts(all_instances)
def setup():
    sudo("add-apt-repository ppa:deadsnakes/ppa -y")
    sudo("apt-get update -y")
    sudo("apt-get install software-properties-common -y")
    sudo("apt-get install python3.6 python3-pip -y")
    sudo("pip3 install --upgrade pip")
    sudo("pip3 install redis")
    run("pip3 install locustio")
    sudo("pip3 install boto3")
    sudo("pip3 install cbor2")

@parallel
@hosts(all_instances)
def setup2():
    sudo("update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.5 1")
    sudo("update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.6 2")


@parallel
@hosts(all_instances)
def copy_files():
    run("mkdir -p ~/locust-load-test")
    rsync_project(
        remote_dir="~/locust-load-test/",
        local_dir="./",
        exclude=("*_local.py", "*.pyc", "primer", "tools", ".gitignore", "fabfile.py", ".git", ".idea", "__pycache__"),
    )


@parallel
@hosts(all_instances)
def clean():
    run("rm -rf ~/locust-load-test/*")


@serial
@hosts(master)
def start_master():
    with cd('locust-load-test'), shell_env(PYTHONPATH="."):
        with settings(warn_only=True): sudo('killall -9v locust')
        sudo("service redis start")
        run("rm -f *.log")
        run("nohup python3 environment_current_sensor_data/queue_delete.py")
        run("(nohup locust -f environment_current_sensor_data/locustfile.py "+locust_opts+"  >& /dev/null < /dev/null &) && sleep 1")


@parallel
@hosts(slaves)
def start_slaves():
    with cd('locust-load-test'), shell_env(PYTHONPATH=".", MASTER_IP=master):
        run("rm -f *.log")
        for x in range(0, 4):
            run("(nohup locust -f environment_current_sensor_data/locustfile.py "+locust_opts+" --slave --master-host="+master+" >& /dev/null < /dev/null &) && sleep 1")

@parallel
@hosts(all_instances)
def killall():
    with settings(warn_only=True): sudo('killall -9v locust')

@serial
@hosts(master)
def start_solo():
    with cd('locust-load-test'), shell_env(PYTHONPATH="."):
        with settings(warn_only=True): sudo('killall -9v locust')
        sudo("service redis start")
        run("rm -f *.log")
        run("nohup python3 environment_current_sensor_data/queue_delete.py")
        run("(nohup locust -f environment_current_sensor_data/locustfile.py "+locust_opts+" --no-reset-stats >& /dev/null < /dev/null &) && sleep 1")

