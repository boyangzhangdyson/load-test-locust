sudo apt-get update
sudo add-apt-repository ppa:deadsnakes/ppa -y
sudo apt-get update
sudo apt-get install software-properties-common -y
sudo apt-get install python3.6 -y
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.5 1
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.6 2
sudo apt-get install python3-pip -y
sudo pip3 install --upgrade pip
sudo pip3 install redis locustio cbor2 boto3
echo '* soft nofile 999999' | sudo tee --append /etc/security/limits.conf
echo '* hard nofile 999999' | sudo tee --append /etc/security/limits.conf
echo 'session required pam_limits.so' | sudo tee --append /etc/pam.d/common-session
echo 'session required pam_limits.so' | sudo tee --append /etc/pam.d/common-session-noninteractive
sudo reboot