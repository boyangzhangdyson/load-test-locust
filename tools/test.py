
import random, time, base64, cbor2, datetime, json

cfg = {
    'aqlm' : { 'min': 0, 'max': 990 },
    'volm' : { 'min': 0, 'max': 990 },
    'no2m' : { 'min': 0, 'max': 990 },
    'p25m' : { 'min': 0, 'max': 9999 },
    'p10m' : { 'min': 0, 'max': 9999 },
    'tmpm' : { 'min': 2430, 'max': 3530 },
    'fnsp' : { 'min': 0, 'max': 100 },
    'humm' : { 'min': 0, 'max': 990 },
    'fnmd' : { 'min': 1, 'max': 5 },
    'fnau' : { 'min': 0, 'max': 60 },
    'fnon' : { 'min': 0, 'max': 60 }
}
maxBuckets = 20

def gen_message():
    data = {}
    for k, v in cfg.items():
        bucket = [0] * maxBuckets
        for i in range(maxBuckets):
            value = random.randint(v['min'], v['max'])
            if value % 50 == 0: value = -1
            bucket[i] = value
        data[k] = bucket

    data['tmsp'] = int(time.time())

    return base64.b64encode(cbor2.dumps(json.dumps(data), datetime_as_timestamp=True, timezone=cbor2.compat.timezone.utc)).decode('utf-8')

bin = gen_message()
msg = {
    "time": datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S%z') + 'Z',
    "bin" : gen_message()
}

dec = base64.b64decode(bin)
o = cbor2.loads(dec)

print(o)

payload = json.dumps(msg)
print(payload)


